# -*- coding: utf-8 -*-
"""
Created on Tue Apr 29 11:29:17 2014

@author: zzhen
"""
import scipy.stats as stat


def var(d):
   """ Calculation of groups of ties"""
   col = len(d)
   nd = 1 # counters of different numbers
   g = []; # number of groups  
   dcount = [] # for counter, cancel repeated numbers
   d.sort() # makes the count possible
   if d[1] == d[0]: 
      dcount.append(d[1])
   for i in range(1,col):    
      if d[i] != d[i-1]:
         nd += 1 # counters of different numbers 
         #print "i", i, "nd", nd
         dcount.append(d[i])
   for i in range(len(dcount)):
      if d.count(dcount[i]) != 1: 
         g.append(d.count(dcount[i]))
   g.sort() # makes the count possible
   unV = col*(col-1)*(col*2+5) # uncorrected V
   cv = 0
   for i in range(0,len(g)):
       cv += g[i]*(g[i]-1)*(g[i]*2+5)    
   return (unV - cv)/18

def ken(d):
    P = 0
    M = 0
    S = 0
    col = len(d)
    for i in range(col-1):
       for j in range((i+1),col):	 
          if d[i]>d[j]: M += 1 
          elif(d[i]<d[j]): P += 1 
    S = P - M
    Z = 0
    V = var(d)
    if S > 0: Z = (S-1)/math.sqrt(V)
    elif S == 0: Z = 0
    else: Z = (S+1)/math.sqrt(V)
    p_vle = 0
    p_vle = (1-pnorm(math.fabs(Z),0,1))*2
    MK = [P, M, S, V, Z, p_vle]
    return MK 
    
if __name__ == "__main__":  
   pnorm = stat.norm.cdf          
   dis = 1.8 # smaller than 2
   tra = 0.0 # a trace value

   d1 = [tra,tra,4,2,16,26,5,dis,5,dis,2,3,dis]
   mk1 = ken(d1)
   print "mk1", mk1
   
   d2 = [4,3,5,3,2,7,3,dis,dis,2,3,5,3]
   mk2 = ken(d2)
   print "mk2", mk2
  
   d3 = [3,2,5,6,18,4,9,dis,dis,tra,dis,dis,tra]
   mk3 = ken(d3)
   print "mk3", mk3
   #V3 = var(d3)
   #print "V3", V3


"""
def var(d):
   # Calculation of groups of ties
   col = len(d)
   nd = 1 # counters of different numbers
   g = []; # number of groups  
   d.sort() # makes the count possible
   for i in range(1,col):
      if d[i] != d[i-1]:
         nd += 1 # counters of different numbers 
         #print "i", i, "nd", nd
         print d[i]
   for j in range(nd):
      if d.count(d[j]) == 2:
         #print "j", j
         #print d
         g.append(d.count(d[j]))            
         #print g
         d.remove(d[j])
         #print d
      elif d.count(d[j]) > 2:
         #print "j", j         
         #print d 
         g.append(d.count(d[j]))
         #print g
         for k in range(d.count(d[j])-1):
             #print "j+k", j+k, d[j+k]
             d.remove(d[j])  

"""